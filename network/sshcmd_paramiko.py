#!/usr/bin/env python3
import paramiko


def ssh_cmd(ip, user, passwd, command):
    client = paramiko.SSHClient()

    # if you are using Unix Like system,
    # next line is better, keys is always the best choices.
    # client.set_missing_host_keys('~/.ssh/known_hosts')

    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    client.connect(ip, username=user, password=passwd)
    ssh_session = client.get_transport().open_session()
    if ssh_session.active:
        ssh_session.exec_command(command)
        line = ssh_session.recv(1024)
        print("[!] Execution Result =>", line)
    return


print('[!] Input your username : ')
username = input()
print('[!] Input your password : ')
passwd = input()
print('[!] Input ip/hostname : ')
host_ip = input()
print('[!] Input the command you wanna execute : ')
cmd = input()

ssh_cmd(host_ip, username, passwd, cmd)
