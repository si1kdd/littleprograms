import urllib2
import json


def byteify(incode):
    if isinstance(incode, dict):
        return {byteify(key): byteify(value) for key, value in incode.iteritems()}
    elif isinstance(incode, list):
        return [byteify(e) for e in incode]
    elif isinstance(incode, unicode):
        return incode.encoding('utf-8')
    else:
        return incode


def get_token(auth_token, auth_url):
    headers = {"Content-type": "application/json",
               "Accept": "application/json;charset=UTF-8"}
    token_url = auth_url + "auth/tokens"
    req = urllib2.Request(token_url, auth_token, headers)
    response = urllib2.urlopen(req)
    token = response.info().getheader("X-Subject-Token")
    response.encoding = 'utf-8'

    response_json_form = json.loads(response.read())
    response_json_form = byteify(response_json_form)
    response_json_form['authtoken'] = token
    return response_json_form


def get_image_id(nova_url, token_id, image_name):
    servers_url = nova_url + '/' + 'images'
    req = urllib2.Request(servers_url)
    req.add_header('X-Auth-Token', token_id)
    response = urllib2.urlopen(req)
    services = json.loads(response.read())['images']
    l = len(services)
    for i in range(0, l):
        if services[i]['name'] == image_name:
            image_id = services[i]['id']
    return image_id


def get_flavor_id(nova_url, token_id, flavor_name):
    servers_url = nova_url + '/' + 'flavors'
    req = urllib2.Request(servers_url)
    req.add_header('X-Auth-Token', token_id)
    response = urllib2.urlopen(req)
    services = json.loads(response.read())['flavors']
    l = len(services)
    for i in range(0, l):
        if services[i]['name'] == flavor_name:
            flavor_id = services[i]['id']
    return flavor_id


def get_nova_endpoint(response):
    services = response['token']['catalog']
    l = len(services)
    # print(l)
    for i in range(0, l):
        if services[i]['name'] == 'nova':
            nova_endpoint = services[i]['endpoints'][0]['url']
    return nova_endpoint


def create_instance(nova_url, token_id, instance_name, image_id, flavor_id):
    headers = {
        "Content-type": "application/json",
        "Accept": "application/json;charset=UTF-8",
        "X-Auth-Token": token_id
    }
    print(headers)
    servers_url = nova_url + "/servers"

    body = {
        "server": {
            "name": instance_name,
            "imageRef": image_id,
            "flavorRef": flavor_id
        }
    }
    body = json.dumps(body)
    req = urllib2.Request(servers_url, body, headers)
    response = urllib2.urlopen(req)
    response_json_form = json.loads(response.read())
    return response_json_form


if __name__ == '__main__':
    # My machine address here.
    auth_url = "http://192.168.0.34:35357/v3/"

    # fill your token information here, just lazy move.
    auth_token = {
        "auth": {
            "identity": {
                "methods": [
                    "password"
                ],
                "password": {
                    "user": {
                        "domain": {"id": "default"},
                        "name": "demo",
                        "password": "123456"
                    }
                }
            },
            "scope": {
                "project": {
                    "domain": {"id": "default"},
                    "name": "demo"
                }
            }
        }
    }
    auth_token = json.dumps(auth_token)
    response = get_token(auth_token, auth_url)
    # print(response)
    token_id = response['authtoken']
    nova_url = get_nova_endpoint(response)
    image_id = get_image_id(nova_url, token_id, 'cirros-0.3.4-x86_64-uec')
    flavor_id = get_flavor_id(nova_url, token_id, 'm1.nano')
    print(create_instance(nova_url, token_id, 'lab3_machine', image_id, flavor_id))
