# Some networking related scripts/tools

* openstack/
    - Update the openstack management scripts, like REST API, auto-reboot etc.

* network_card_speed.c
    - a tiny program to check the NIC speed support.

* sshcmd_paramiko.py
    - a tiny python script to do the SSH stuff.

* small_netstat/
    - a small netstat like program.


* To be continued......

