#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <linux/sockios.h>
#include <netinet/in.h>
#include <sys/ioctl.h>
#include <sys/socket.h>

#include <linux/ethtool.h>
#include <linux/if.h>

/*
 * As no uapi header provides a definition of struct sockaddr, inclusion
 * of <sys/socket.h> seems to be the most conservative and the only safe
 * fix available.
 *
 * All current users of <linux/if.h> are very likely to be including
 * <sys/socket.h> already because the latter is the sole provider
 * of struct sockaddr definition in libc, so adding a uapi header
 * with a definition of struct sockaddr would create a potential
 * conflict with <sys/socket.h>.
 *
 * Replacing struct sockaddr in the definition of struct ifreq with
 * a different type would create a potential incompatibility with current
 * users of struct ifreq who might rely on ifru_addr et al members being
 * of type struct sockaddr.
 *
 */

int main(int argc, char* argv[])
{
        char* network_card = NULL;

        if (argc < 2) {
                fprintf(stderr,
                        "[!] Please input the network card device name\n");
                exit(-1);
        }
        network_card = argv[1];

        int sock = socket(PF_INET, SOCK_DGRAM, IPPROTO_IP);
        if (sock < 0) {
                perror("socket");
                exit(-1);
        }

        struct ifreq ifr;
        struct ethtool_cmd edata;

        strncpy(ifr.ifr_name, network_card, sizeof(ifr.ifr_name));

        ifr.ifr_data = &edata;
        edata.cmd = ETHTOOL_GSET;

        int rc = ioctl(sock, SIOCETHTOOL, &ifr);
        if (rc < 0) {
                perror("ioctl");
                exit(-1);
        }

        switch (ethtool_cmd_speed(&edata)) {
                case SPEED_10:
                        printf("10Mbps\n");
                        break;
                case SPEED_100:
                        printf("100Mbps\n");
                        break;
                case SPEED_1000:
                        printf("1Gbps\n");
                        break;
                case SPEED_2500:
                        printf("2.5Gbps\n");
                        break;
                case SPEED_10000:
                        printf("10Gbps\n");
                        break;
                default:
                        printf("Speed returned is %d\n", edata.speed);
        }
        return 0;
}
