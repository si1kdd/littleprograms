#include <regex.h>
#include <stdio.h>
#include <string.h>

int regex_filter(const char *conn, char *filter_prog[], int len)
{
        regex_t rgx;
        int z = 1;
        for (int i = 0; i < len; ++i) {
                if (regcomp(&rgx, filter_prog[i], 0) != 0) {
                        printf("fail to regex compile : %s\n", conn);
                        return -1;
                }
                z = regexec(&rgx, conn, 0, NULL, 0);
                if (z == 0) {
                        return 1;
                }
        }
        regfree(&rgx);
        return -3; // don't know.
}
