#include <ctype.h>
#include <dirent.h>
#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#define MAX_SYMBOL 10

int get_fd_len(const char *dname, int len)
{
        int t = 0;
        for (int i = 0; i < len; ++i)
                if (isdigit(dname[i]))
                        t += 1;
        return t;
}

void get_program_name(const char *pid, char p[])
{
        char prog_path[11 + strlen(pid)];
        sprintf(prog_path, "/proc/%s/comm", pid);
        FILE *fp = fopen(prog_path, "r");
        char prog_name[256], ret[256];
        if (fp) {
                fgets(prog_name, 256, fp);
                size_t nl = strlen(prog_name) - 1;
                if (prog_name[nl] == '\n')
                        prog_name[nl] = '\0';
                // printf("%s/%s\n", pid, prog_name);
                sprintf(ret, "%s/%s", pid, prog_name);
                strcpy(p, ret);
        }
        fclose(fp);
}

void get_progs_and_args(const char *pid, char p[])
{
        char prog_path[15 + strlen(pid)];
        sprintf(prog_path, "/proc/%s/cmdline", pid);

        unsigned char cmd[1024];
        FILE *fp = fopen(prog_path, "rb");
        if (fp != NULL) {
                int byte = fread(cmd, 1, 1024, fp);
                for (int i = 0; i < byte; ++i)
                        if (cmd[i] == '\0' && cmd[0] != '\0')
                                cmd[i] = ' ';
                cmd[byte] = '\0';
                // printf("%s\n", cmd);

                if (cmd[0] == '/')
                        sprintf(p, "%s%s", pid, cmd);
                else
                        sprintf(p, "%s/%s", pid, cmd);
        }
        fclose(fp);
}

void check_path_inode(const char *fd_path, unsigned int inode, const char *pid,
                      char p[])
{
        char buf[256], c_inode[256];
        sprintf(c_inode, "%d", inode);
        if (readlink(fd_path, buf, sizeof(buf)) != -1) {
                if (strstr(buf, c_inode) != NULL) {
                        // printf("%s => %s : %s\n", fd_path, buf, c_inode);
                        // get_program_name(pid, p); // not comm. Orz.
                        get_progs_and_args(pid, p);
                }
        }
}

void get_fd_sym_link(DIR *fdp, struct dirent *dent, char *fd_path,
                     unsigned int inode, char p[])
{
        struct dirent *fd_dent;
        while ((fd_dent = readdir(fdp)) != NULL) {
                if (strcmp(fd_dent->d_name, ".") != 0 &&
                    strcmp(fd_dent->d_name, "..") != 0 &&
                    strlen(fd_dent->d_name) != 0) {
                        sprintf(fd_path, "/proc/%s/fd/%s", dent->d_name,
                                fd_dent->d_name);
                        struct stat st_dname;
                        int re = stat(fd_path, &st_dname);
                        if (re != -1 && S_ISSOCK(st_dname.st_mode))
                                check_path_inode(fd_path, inode, dent->d_name,
                                                 p);
                }
        }
}

void get_fd_path(DIR *dirp, struct dirent *dent, unsigned int inode, char p[])
{
        DIR *fdp;
        while ((dent = readdir(dirp)) != NULL) {
                int len = strlen(dent->d_name);
                int fd_len = get_fd_len(dent->d_name, len);
                if (fd_len) {
                        char fd_path[fd_len + 11 + MAX_SYMBOL];
                        sprintf(fd_path, "/proc/%s/fd",
                                dent->d_name); // printf("%s\n", fd_path);

                        if ((fdp = opendir(fd_path)) != NULL) {
                                get_fd_sym_link(fdp, dent, fd_path, inode, p);
                        } else {
                                if (errno == EACCES) {
                                        perror("[X] failed to open /proc/fd/1, \
                                                please use run this program in super user privileges ....\n");
                                        exit(errno);
                                }
                        }
                        closedir(fdp);
                }
        }
}

void get_pid(unsigned int inode, char ret[])
{
        DIR *dirp;
        if ((dirp = opendir("/proc")) == NULL) {
                if (errno == EACCES) {
                        perror("failed to open /proc, please use sudo run this "
                               "program...\n");
                        exit(errno);
                }
        } else {
                struct dirent dent;
                get_fd_path(dirp, &dent, inode, ret);
                // printf("%s [here is get_pid]\n", ret);
                closedir(dirp);
        }
}
