/*
 * A small netstat like programs.
 *
 */

#include <arpa/inet.h>
#include <errno.h>
#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>

#include "netstat.h"

static inline void display_usage()
{
        printf("$ ./hw1 [-t|--tcp] [-u|--udp] [filter-string] \n");
}

static inline void stat_line()
{
        printf("%-5s %-24s%-24s%-10s\n", "Proto", "Local Address",
               "Foreign Address", "PID/Program name and arguments");
}

static inline void print_conn(const char *l_ip, const char *r_ip, short i,
                              const char *p)
{
        if (i == 0)
                printf("%-5s %-23s %-23s %-10s\n", "tcp", l_ip, r_ip, p);
        else if (i == 1)
                printf("%-5s %-23s %-23s %-10s\n", "tcp6", l_ip, r_ip, p);
        else if (i == 2)
                printf("%-5s %-23s %-23s %-10s\n", "udp", l_ip, r_ip, p);
        else if (i == 3)
                printf("%-5s %-23s %-23s %-10s\n", "udp6", l_ip, r_ip, p);
        else
                printf("\n");
}

static char *byte2addr4(struct sockaddr_in *s_addr, char *byte_addr,
                        unsigned int port)
{
        /* s_addr just a long */
        sscanf(byte_addr, "%X",
               &((struct sockaddr_in *)s_addr)->sin_addr.s_addr);
        ((struct sockaddr_in *)s_addr)->sin_family = AF_INET;

        char p[11]; // 32 bit port.
        if (port != 0)
                sprintf(p, "%d", port);
        else
                sprintf(p, "%s", "*");

        char ipv4[INET_ADDRSTRLEN];
        inet_ntop(AF_INET, &(((struct sockaddr_in *)s_addr)->sin_addr), ipv4,
                  INET_ADDRSTRLEN);
        char *r = (char *)malloc(sizeof(char) * (INET_ADDRSTRLEN + strlen(p)));
        sprintf(r, "%s:%s", ipv4, p);
        // strncpy(r, ipv4, INET_ADDRSTRLEN); strncat(r, ":", 2); strncat(r, p,
        // strlen(p)); //Orz.

        ((struct sockaddr_in *)s_addr)->sin_family = AF_INET;
        return r;
}

static char *byte2addr6(struct sockaddr_in6 *s_addr, char *byte_addr,
                        unsigned int port)
{
        /* s6_addr is a char [16] array*/
        struct in6_addr in6;
        int len = sscanf(
            byte_addr, "%2hhx%2hhx%2hhx%2hhx%2hhx%2hhx%2hhx%2hhx%2hhx%2hhx%"
                       "2hhx%2hhx%2hhx%2hhx%2hhx%2hhx",
            &in6.s6_addr[3], &in6.s6_addr[2], &in6.s6_addr[1], &in6.s6_addr[0],
            &in6.s6_addr[7], &in6.s6_addr[6], &in6.s6_addr[5], &in6.s6_addr[4],
            &in6.s6_addr[11], &in6.s6_addr[10], &in6.s6_addr[9],
            &in6.s6_addr[8], &in6.s6_addr[15], &in6.s6_addr[14],
            &in6.s6_addr[13], &in6.s6_addr[12]);
        char ipv6[32]; // POSIX use 46 for real address.
        if (len == 16)
                inet_ntop(AF_INET6, &in6, ipv6, 32);

        char p[11]; // 32 bit port.
        if (port != 0)
                sprintf(p, "%d", port);
        else
                sprintf(p, "%s", "*");
        char *r = (char *)malloc(sizeof(char) * (32 + strlen(p)));
        sprintf(r, "%s:%s", ipv6, p);

        ((struct sockaddr_in6 *)s_addr)->sin6_family = AF_INET6;
        return r;
}

static void parse_files(short d_flag, short t_flag, short u_flag, short p_flag,
                        char *filter_prog[], int fg_len)
{
        // tcp and udp columns.
        int sl;
        unsigned int l_port, r_port, state, tx_queue, rx_queue;
        unsigned int timer_run, time_len, retr, uid, timeout, inode;
        char useless_str[512] = {0};
        useless_str[0] = '\0';

        char line[1024] = {0};
        char l_byte_addr[64], r_byte_addr[64];

        FILE *fp;
        char *l_ip = NULL, *r_ip = NULL;

        short i = 0, size = 4;
        if (t_flag && !u_flag) {
                i = 0, size = 2;
        } else if (u_flag && !t_flag) {
                i = 2, size = 4;
        } else if (!t_flag && !u_flag && !d_flag)
                return;

        while (i < size) {
                if (i == 0) {
                        printf("%s\n", _TCP_CONN);
                        stat_line();
                } else if (i == 2) {
                        if (d_flag)
                                printf("\n");
                        printf("%s\n", _UDP_CONN);
                        stat_line();
                }

                fp = fopen(default_path[i], "r");
                if (fp != 0) {
                        int col = 0;
                        fgets(line, 1024, fp); // first line useless.
                        while (fgets(line, 1024, fp) != NULL) {
                                col = sscanf(
                                    line, "%d: %64[0-9A-Fa-f]:%X "
                                          "%64[0-9A-Fa-f]:%X %X %X:%X %X:%X "
                                          "%X %u %u %u %512s\n",
                                    &sl, l_byte_addr, &l_port, r_byte_addr,
                                    &r_port, &state, &tx_queue, &rx_queue,
                                    &timer_run, &time_len, &retr, &uid,
                                    &timeout, &inode, useless_str);
                                if (col == 15 && (i % 2 == 0)) {
                                        struct sockaddr_in sl_addr, sr_addr;
                                        l_ip = byte2addr4(&sl_addr, l_byte_addr,
                                                          l_port);
                                        r_ip = byte2addr4(&sr_addr, r_byte_addr,
                                                          r_port);
                                        char pid_prog[256] = {0};
                                        get_pid(inode, pid_prog);
                                        if (!p_flag) {
                                                print_conn(l_ip, r_ip, i,
                                                           pid_prog);
                                        } else if (p_flag) {
                                                int p = regex_filter(
                                                    pid_prog, filter_prog,
                                                    fg_len);
                                                if (p == 1) {
                                                        print_conn(l_ip, r_ip,
                                                                   i, pid_prog);
                                                }
                                        }
                                        free(l_ip);
                                        free(r_ip);
                                        // printf("%s\n", pid_prog);
                                } else if (col == 15) {
                                        struct sockaddr_in6 sl_addr, sr_addr;
                                        l_ip = byte2addr6(&sl_addr, l_byte_addr,
                                                          l_port);
                                        r_ip = byte2addr6(&sr_addr, r_byte_addr,
                                                          r_port);
                                        char pid_prog[256] = {0};
                                        get_pid(inode, pid_prog);
                                        if (!p_flag) {
                                                print_conn(l_ip, r_ip, i,
                                                           pid_prog);
                                        } else if (p_flag) {
                                                int p = regex_filter(
                                                    pid_prog, filter_prog,
                                                    fg_len);
                                                if (p == 1) {
                                                        print_conn(l_ip, r_ip,
                                                                   i, pid_prog);
                                                }
                                        }
                                        free(l_ip);
                                        free(r_ip);
                                }
                        }
                } else {
                        perror("Failed to open /proc/net/ files");
                        exit(errno);
                }
                fclose(fp);
                i++;
        }
}

int main(int argc, char *argv[])
{
        short d_flag = 0;
        short u_flag = 0, t_flag = 0, p_flag = 0;

        if (argc < 2)
                d_flag = 1;

        static const struct option long_options[] = {
            {"tcp", no_argument, 0, 't'},
            {"udp", no_argument, 0, 'u'},
            {"help", no_argument, 0, 'h'},
            {0, 0, 0, 0}};

        int c = 0;
        while (1) {
                c = getopt_long(argc, argv, "tuh", long_options, NULL);
                if (c == -1)
                        break;

                switch (c) {
                case 'h':
                        display_usage();
                        break;
                case 't':
                        t_flag = 1;
                        break;
                case 'u':
                        u_flag = 1;
                        break;
                case '?':
                        display_usage();
                        t_flag = u_flag = d_flag = 0;
                        break;
                default:
                        t_flag = u_flag = d_flag = 0;
                        break;
                }
        }

        int fg = 0;
        while (argv[optind] != NULL) {
                // printf("%s\n", argv[optind++]);
                filter_prog[fg++] = argv[optind++];
        }
        if (fg > 0)
                p_flag = 1;
        /* start parsing */
        parse_files(d_flag, t_flag, u_flag, p_flag, filter_prog, fg);
        return 0;
}
