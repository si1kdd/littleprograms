#ifndef __NET_STAT_H_
#define __NET_STAT_H_

#define _TCP_CONN "List of TCP connections:"
#define _UDP_CONN "List of UDP connections:"
#define TCP4_PATH "/proc/net/tcp"
#define UDP4_PATH "/proc/net/udp"
#define TCP6_PATH "/proc/net/tcp6"
#define UDP6_PATH "/proc/net/udp6"

const char *default_path[] = {TCP4_PATH, TCP6_PATH, UDP4_PATH, UDP6_PATH};

char *filter_prog[256];

void get_pid(unsigned int inode, char p[]);
int regex_filter(const char *conn, char *filter_prog[], int len);

#endif
