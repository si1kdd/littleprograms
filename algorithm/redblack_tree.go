package rbtree

import (
        "fmt"
        "errors"
)

// define Red, Black flags
const R bool = true
const B bool = false

type RBtree struct {
        root *node
        size int
        compare func(i, j interface{}) bool
}

type Node struct {
	key  interface{}
	data interface{}

	color bool
	parent *Node
	right *Node
	left  *Node
}

func (t *RBtree) Insert(key, data interface{}) {
	t.size++
	new_node := new(Node)

	new_node.color = R
	new_node.data = data
	new_node.parent = nil
	new_node.right = nil
	new_node.left = nil

	// Find the correct position.
	if t.root == nil {
		t.root = new_node
	}
	else {
		tmp_node := t.root
		for true {
		}
	}
}

// To be continued......
	}

}


