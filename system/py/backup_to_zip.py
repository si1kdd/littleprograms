#!/usr/bin/env python3

# Backup an entire folder and its contents
# into a ZIP file whose filename increments

import zipfile
import os


def backupToZip(folder):
    folder = os.path.abspath(folder)
    number = 1
    while True:
        zipFilename = os.path.basename(folder) + '_' + str(number) + '.zip'
        if not os.path.exists(zipFilename):
            break
        number = number + 1

    print('[*] Creating %s ...' % (zipFilename))
    backupZip = zipfile.ZipFile(zipFilename, 'w')

    for folderName, subfolders, filenames in os.walk(folder):
        print('[*] Adding files in %s ...' % (folderName))
        backupZip.write(folderName)
        for filename in filenames:
            new_base = os.path.basename(folder) + '_'
            if filename.startswith(new_base) and filename.endswith('.zip'):
                continue
            backupZip.write(os.path.join(folderName, filename))
    backupZip.close()
    print('[*] Done.')


print("[*] Please input the path: ")
path = input()
backupToZip(str(path))
