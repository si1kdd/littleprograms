import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn import preprocessing
from sklearn.neighbors import KNeighborsClassifier

# check some data which not follow attribute information.
def factor_sum(x):
    ret = dict()
    for i in x.unique():
        if pd.isnull(i):
            ret["NaN"] = x.isnull().sum()
        else:
            ret[i] = np.sum(x == i)
    return ret


def read_and_write_data():
    data = pd.read_excel("./default of credit card clients.xls", sheet=0, skiprows=1, header=0, sheetname='Data')

    target = 'default payment next month'
    df = data.copy()
    print(df.columns) #Check.

    # Remap EDUCATION.
    df["EDUCATION"] = df["EDUCATION"].map({0: np.NaN, 1:1, 2:2, 3:3, 4:np.NaN, 5:np.NaN, 6:np.NaN})
    # print(factor_sum(df["EDUCATION"])) #check.

    # Remap MARRIAGE
    df["MARRIAGE"] = df["MARRIAGE"].map({0:np.NaN, 1:1, 2:2, 3:np.NaN})
    # print(factor_sum(df["MARRIAGE"])) #check.

    print("Useless data: ", df.isnull().sum())

    p = df.columns.drop(['ID', target])
    X = np.asarray(df[p]) # The main matrix.
    y = np.asarray(df[target])

    X_train, X_test, Y_train, Y_test = train_test_split(X, y, test_size=0.3, random_state=0, stratify=y)
    print("Training Data: ", X_train.shape)
    print("Test  Shape: ", X_test.shape)
    print("Training Target Shape: " ,Y_train.shape)
    print("Test Target Shape: " ,Y_test.shape)

    print("Transform to csv files")
    np.savetxt("./X_train.txt", X_train, delimiter=",", fmt='%2.0f')
    np.savetxt("./X_test.txt", X_test, delimiter=",", fmt='%2.0f')
    np.savetxt("./Y_train.txt", Y_train, delimiter=",", fmt='%2.0f')
    np.savetxt("./Y_test.txt", Y_test, delimiter=",", fmt='%2.0f')

    print("Data Splits Done")

# reduce the level of data.
read_and_write_data()
