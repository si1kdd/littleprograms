from sklearn import preprocessing
from sklearn import metrics
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import accuracy_score

import pandas as pd
import numpy as np
from numpy import isnan

X_train = np.loadtxt('./X_train.txt', delimiter=',')
Y_train = np.loadtxt('./Y_train.txt', delimiter=',')
X_test = np.loadtxt('./X_test.txt', delimiter=',')
Y_test = np.loadtxt('./Y_test.txt', delimiter=',')

x = isnan(X_train)
y = isnan(Y_train)

X_train[x] = 100
Y_train[y] = 100

x = isnan(X_test)
y = isnan(Y_test)

X_test[x] = 100
Y_test[y] = 100

# normalize the data attributes
normalize_X = preprocessing.normalize(X_train)
standardized_X = preprocessing.scale(X_train)
# print(standardized_X)

# Random Forest.
model = RandomForestClassifier()
model.fit(X_train,Y_train)
# print(model.feature_importances_)

expected = Y_train
predicted = model.predict(X_train)
print(metrics.classification_report(expected, predicted))
print("\nConfusion Matrix:")

print(metrics.confusion_matrix(expected, predicted))

train_score = model.score(X_train, Y_train)

# Accuracy Score:
print("\nAccuracy Score:")
print(accuracy_score(Y_train, predicted))

print("\nTest Set Accuracy Score:")
print(model.score(X_test, Y_test))
