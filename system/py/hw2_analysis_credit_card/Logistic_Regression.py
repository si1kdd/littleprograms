from sklearn import preprocessing
from sklearn import metrics
from sklearn.ensemble import ExtraTreesClassifier
from sklearn.metrics import roc_curve, auc

import pandas as pd
import numpy as np
from numpy import isnan
import matplotlib.pyplot as plt
plt.style.use('ggplot')

X_train = np.loadtxt('./X_train.txt', delimiter=',')
Y_train = np.loadtxt('./Y_train.txt', delimiter=',')
X_test = np.loadtxt('./X_test.txt', delimiter=',')
Y_test = np.loadtxt('./Y_test.txt', delimiter=',')

x = isnan(X_train)
y = isnan(Y_train)

X_train[x] = 100
Y_train[y] = 100

x = isnan(X_test)
y = isnan(Y_test)

X_test[x] = 100
Y_test[y] = 100


#Feature Selection
model = ExtraTreesClassifier()
feature_matrix = model.fit(X_train,Y_train).transform(X_train)

# print(model.feature_importances_)

# Logistic Regression:
expected = Y_train
predicted = model.predict(X_train)
print(metrics.classification_report(expected, predicted))
print(metrics.confusion_matrix(expected, predicted))

false_positive_rate, true_positive_rate, thresholds = roc_curve(Y_test, model.predict_proba(X_test)[:,1])
roc_auc = auc(false_positive_rate, true_positive_rate)

plt.title('ROC')
plt.plot(false_positive_rate, true_positive_rate, 'b', label='AUC = %0.2f'% roc_auc)
plt.legend(loc='lower right')
plt.show()

print("Score: Train Set and Test Set")
print(model.score(X_train, Y_train), '\n')
print(model.score(X_test, Y_test))
