from sklearn import preprocessing
from sklearn import metrics
from sklearn.metrics import confusion_matrix
from sklearn.neighbors import KNeighborsClassifier
from sklearn.preprocessing import Imputer

import pandas as pd
import numpy as np
from numpy import *

import pandas as pd
import numpy as np
from numpy import isnan

X_train = np.loadtxt('./X_train.txt', delimiter=',')
Y_train = np.loadtxt('./Y_train.txt', delimiter=',')

x = isnan(X_train)
y = isnan(Y_train)

X_train[x] = 100
Y_train[y] = 100

# normalize the data attributes
normalize_X = preprocessing.normalize(X_train)
standardized_X = preprocessing.scale(X_train)
# print(standardized_X)

# K-NN
model = KNeighborsClassifier()
model.fit(X_train, Y_train)
print(model.score(X_train, Y_train))
