from sklearn import preprocessing
from sklearn import metrics
from sklearn.naive_bayes import GaussianNB
from sklearn.metrics import confusion_matrix
from sklearn.metrics import accuracy_score

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from numpy import isnan

X_train = np.loadtxt('./X_train.txt', delimiter=',')
Y_train = np.loadtxt('./Y_train.txt', delimiter=',')
X_test = np.loadtxt('./X_test.txt', delimiter=',')
Y_test = np.loadtxt('./Y_test.txt', delimiter=',')

x = isnan(X_train)
y = isnan(Y_train)

X_train[x] = 100
Y_train[y] = 100

x = isnan(X_test)
y = isnan(Y_test)

X_test[x] = 100
Y_test[y] = 100

# normalize the data attributes
normalize_X = preprocessing.normalize(X_train)
standardized_X = preprocessing.scale(X_train)
# print(standardized_X)

#Feature Selection
model = GaussianNB()
model.fit(X_train,Y_train)
# print(model.feature_importances_)

# Naive Bayes:
expected = Y_train
predicted = model.predict(X_train)

print("\nConfusion Matrix:")
print(metrics.confusion_matrix(expected, predicted))

# Accuracy Score:
print("\nTrain Set Accuracy Score:")
print(accuracy_score(Y_train, predicted))

print("\nTest Set Accuracy Score:")
print(model.score(X_test, Y_test))

cm = metrics.confusion_matrix(expected, predicted)

fig = plt.figure()
ax = fig.add_subplot(111)
cax = ax.matshow(cm)
plt.title('Confusion matrix of the clasifier')
fig.colorbar(cax)
plt.show()

