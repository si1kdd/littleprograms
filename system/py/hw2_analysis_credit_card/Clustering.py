import numpy as np
from numpy import isnan
import matplotlib.pyplot as plt
import pandas as pd
import itertools
from sklearn.cluster import KMeans

X_train = np.loadtxt('./X_data.txt', delimiter=',')
Y_train = np.loadtxt('./Y_data.txt', delimiter=',')

x = isnan(X_train)
y = isnan(Y_train)

X_train[x] = 100
Y_train[y] = 100

plt.figure(figsize=(12,12))
kmeans = KMeans(n_clusters = 3)
preds = kmeans.fit_predict(X_train)

plt.scatter(X_train[:, 0], X_train[:, 1], X_train[:, 2], c=preds)
plt.plot()
plt.show()
