from sklearn import preprocessing
from sklearn.svm import SVC
from sklearn.metrics import accuracy_score

import numpy as np
from numpy import isnan

X_train = np.loadtxt('./X_train.txt', delimiter=',')
Y_train = np.loadtxt('./Y_train.txt', delimiter=',')
X_test = np.loadtxt('./X_test.txt', delimiter=',')
Y_test = np.loadtxt('./Y_test.txt', delimiter=',')

x = isnan(X_train)
y = isnan(Y_train)

X_train[x] = 100
Y_train[y] = 100

x = isnan(X_test)
y = isnan(Y_test)

X_test[x] = 100
Y_test[y] = 100
# normalize the data attributes
normalize_X = preprocessing.normalize(X_train)
standardized_X = preprocessing.scale(X_train)
# print(standardized_X)

# Training.
svc = SVC(kernel='rbf').fit(X_train, Y_train)

# Default C = 100, gamma = 0.

expected = Y_train
predicted = svc.predict(X_train)
# Accuracy Score:
print("\nAccuracy Score:")
print(accuracy_score(Y_train, predicted))

print("\nTest Set Accuracy Score:")
print(svc.score(X_test, Y_test))

# Change C and gamma
print("\n C = 10, gamma = 0.001")
svc_2 = SVC(kernel='rbf', C=10, gamma=0.001).fit(X_train, Y_train)
train_score = svc_2.score(X_train, Y_train)
print("\nAccuracy Score:")
print(train_score)

print("\nTest Set Accuracy Score:")
test_score = svc_2.score(X_test, Y_test)
print(test_score)

