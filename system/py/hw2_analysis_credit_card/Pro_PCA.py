from sklearn.decomposition import RandomizedPCA
from sklearn import preprocessing
from sklearn import metrics
from itertools import cycle

import pandas as pd
import numpy as np
import matplotlib.pylab as plt
from numpy import isnan

X_train = np.loadtxt('./X_train.txt', delimiter=',')
Y_train = np.loadtxt('./Y_train.txt', delimiter=',')
X_test = np.loadtxt('./X_test.txt', delimiter=',')
Y_test = np.loadtxt('./Y_test.txt', delimiter=',')

x = isnan(X_train)
y = isnan(Y_train)

X_train[x] = 100
Y_train[y] = 100

x = isnan(X_test)
y = isnan(Y_test)

X_test[x] = 100
Y_test[y] = 100
# normalize the data attributes
normalize_X = preprocessing.normalize(X_train)
standardized_X = preprocessing.scale(X_train)
# print(standardized_X)


pca = RandomizedPCA(n_components=3)
X_pca = pca.fit_transform(X_train)

print(X_pca.shape)

colors = ['b', 'g', 'r', 'c', 'm', 'y', 'k']
markers = ['+', 'o', '$', 'v', '<', '>', 'D', 'h', 's']

print(Y_train)
for i, c, m in zip(np.unique(Y_train), cycle(colors), cycle(markers)):
    plt.scatter(X_pca[Y_train == i, 0], X_pca[Y_train == i, 1], c=c, marker=m, label=i, alpha=0.5)

plt.legend(loc='best')
plt.show()
