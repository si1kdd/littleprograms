# Programs about Operation system.

## C/C++:
* monitor:
    - a simple Linux resource monitor.

* CG:
    - a simple OpenMP homework.

* Parallel_Works:
    - some simple CUDA and OpenCL programs.

## Python:

* hw_2_credit_card: a small
    - a simple data mining homework, implement some DM related algorithm like SVM, KNN etc.
