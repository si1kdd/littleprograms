/* *******************************************************************
 *
 * DESCRIPTION :
 * Parallel Concurrent Wave Equation
 * This program implements the concurrent wave equation
 *
 * *******************************************************************
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <cuda.h>

#define MAXPOINTS 1000000
#define MAXSTEPS 1000000
#define MINPOINTS 20
#define PI 3.14159265

void check_param(void);
void printfinal(void);
__global__ void update(float *valuesArray,
                       int tpoints,
                       int nsteps,
                       int blockSize);

int blockSize = 1024;
int nsteps, tpoints;
float values[MAXPOINTS + 2];

// oldval[MAXPOINTS + 2] , newval[ MAXPOINTS + 2];
void check_param(void)
{
    char tchar[20];
    /* check number of points , number of iterations */
    while ((tpoints < MINPOINTS) || (tpoints > MAXPOINTS)) {
        printf(" Enter number of points along vibrating string [%d -%d]: ",
               MINPOINTS, MAXPOINTS);
        scanf("%s", tchar);
        tpoints = atoi(tchar);
        if ((tpoints < MINPOINTS) || (tpoints > MAXPOINTS))
            printf(" Invalid . Please enter value between %d and %d\n",
                   MINPOINTS, MAXPOINTS);
    }
    while ((nsteps < 1) || (nsteps > MAXSTEPS)) {
        printf(" Enter number of time steps [1 -%d]: ", MAXSTEPS);
        scanf("%s", tchar);

        nsteps = atoi(tchar);
        if ((nsteps < 1) || (nsteps > MAXSTEPS))
            printf(" Invalid . Please enter value between 1 and %d\n",
                   MAXSTEPS);
    }
    printf(" Using points = %d, steps = %d\n", tpoints, nsteps);
}

/* *********************************************************************
* Combine all the calculate fucntion than run them on device.
******************************************************************** */
__global__ void update(float *valuesArray,
                       int tpoints,
                       int nsteps,
                       int blockSize)
{
    // do_math function
    float dtime, c, dx, tau, sqtau;
    dtime = 0.3;
    c = 1.0;
    dx = 1.0;
    tau = (c * dtime / dx);
    sqtau = tau * tau;

    // init_line function
    int i, j = threadIdx.x + 1;
    float x, fac, tmp;
    float value_c, oldval_c, newval_c;

    int k = blockIdx.x * blockSize + j;

    fac = 2.0 * PI;
    tmp = tpoints - 1;
    x = (k - 1) / tmp;
    // faster sin function
    value_c = __sinf(fac * x);
    oldval_c = value_c;

    // update points function
    if (k <= tpoints) {
        for (i = 1; i <= nsteps; i++) {
            if ((k == 1) || (k == tpoints))
                newval_c = 0.0;
            else
                newval_c =
                    (2.0 * value_c) - oldval_c + (sqtau * (-2.0) * value_c);
            oldval_c = value_c;
            value_c = newval_c;
        }
        valuesArray[k] = value_c;
    }
}


/* *********************************************************************
* Print final results
******************************************************************** */
void printfinal()
{
    int i;
    for (i = 1; i <= tpoints; i++) {
        printf("%6.4f ", values[i]);
        if (i % 10 == 0)
            printf("\n");
    }
}

/* *********************************************************************
* Main program
******************************************************************** */
int main(int argc, char *argv[])
{
    sscanf(argv[1], "%d", &tpoints);
    sscanf(argv[2], "%d", &nsteps);
    check_param();
    printf(" Initializing points on the line ...\n");
    // init_line ();
    printf(" Updating all points for all time steps ...\n");

    float *valuesArray;
    cudaMalloc((void **) &valuesArray, (sizeof(float) * (tpoints + 1)));
    int blockNumber;
    if (tpoints % blockSize == 0) {
        blockNumber = tpoints / blockSize;
        update<<<blockNumber, blockSize>>>(valuesArray, tpoints, nsteps,
                                           blockSize);
    } else {
        blockNumber = tpoints / blockSize + 1;
        update<<<blockNumber, blockSize>>>(valuesArray, tpoints, nsteps,
                                           blockSize);
    }

    // at last copy the array from device memory.
    cudaMemcpy(values, valuesArray, (sizeof(float) * (tpoints + 1)),
               cudaMemcpyDeviceToHost);
    cudaFree(valuesArray);

    printf(" Printing final results ...\n");
    printfinal();
    printf("\nDone .\n\n");

    return 0;
}
