#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cmath>
#include <fstream>
#include <iostream>

#ifdef MAC
#include <OpenCL/cl.h>
#else
#include <CL/cl.h>
#endif

const char *KernelSource =
    "__kernel\n"
    "void histogram(__global unsigned int *image, __global unsigned int "
    "*result) {\n"
    "int color_id = get_global_id(0);"
    "atomic_inc(&result[(color_id % 3) * 256 + image[color_id]]);\n"
    "}\n";

int main(int argc, char const *argv[])
{
    unsigned int i = 0, a, input_size;
    std::fstream inFile("input", std::ios_base::in);
    std::ofstream outFile("0216095.out", std::ios_base::out);
    inFile >> input_size;

    unsigned int *image = new unsigned int[input_size];
    unsigned int *histogram_results = new unsigned int[256 * 3];
    memset(histogram_results, 0x0, 256 * 3 * sizeof(unsigned int));

    while (inFile >> a) {
        image[i++] = a;
    }

    cl_int status;
    cl_uint PlatFormNums = 0;

    status = clGetPlatformIDs(0, NULL, &PlatFormNums);
    cl_platform_id *platforms = new cl_platform_id[PlatFormNums];
    status = clGetPlatformIDs(PlatFormNums, platforms, NULL);

    cl_uint Devices_num = 0;

    status =
        clGetDeviceIDs(platforms[0], CL_DEVICE_TYPE_ALL, 0, NULL, &Devices_num);
    cl_device_id *devices = new cl_device_id[Devices_num];
    status = clGetDeviceIDs(platforms[0], CL_DEVICE_TYPE_ALL, Devices_num,
                            devices, NULL);

    cl_context context;
    context = clCreateContext(NULL, Devices_num, devices, NULL, NULL, &status);

    cl_command_queue Command_queue =
        clCreateCommandQueue(context, devices[0], 0, &status);

    cl_mem bufImg =
        clCreateBuffer(context, CL_MEM_READ_ONLY,
                       input_size * sizeof(unsigned int), NULL, &status);
    cl_mem bufRst =
        clCreateBuffer(context, CL_MEM_WRITE_ONLY,
                       256 * 3 * sizeof(unsigned int), NULL, &status);
    status = clEnqueueWriteBuffer(Command_queue, bufImg, CL_FALSE, 0,
                                  input_size * sizeof(unsigned int), image, 0,
                                  NULL, NULL);
    status = clEnqueueWriteBuffer(Command_queue, bufRst, CL_FALSE, 0,
                                  256 * 3 * sizeof(unsigned int),
                                  histogram_results, 0, NULL, NULL);

    cl_program program = clCreateProgramWithSource(
        context, 1, (const char **) &KernelSource, NULL, &status);

    status = clBuildProgram(program, Devices_num, devices, NULL, NULL, NULL);

    cl_kernel kernel;
    kernel = clCreateKernel(program, "histogram", &status);

    status = clSetKernelArg(kernel, 0, sizeof(cl_mem), &bufImg);
    status = clSetKernelArg(kernel, 1, sizeof(cl_mem), &bufRst);

    size_t GlobalSize[1];
    GlobalSize[0] = input_size;
    status = clEnqueueNDRangeKernel(Command_queue, kernel, 1, NULL, GlobalSize,
                                    NULL, 0, NULL, NULL);
    clEnqueueReadBuffer(Command_queue, bufRst, CL_TRUE, 0,
                        256 * 3 * sizeof(unsigned int), histogram_results, 0,
                        NULL, NULL);

    for (unsigned int i = 0; i < 256 * 3; ++i) {
        if (i % 256 == 0 && i != 0)
            outFile << std::endl;
        outFile << histogram_results[i] << ' ';
    }
    // careful!!!
    clReleaseKernel(kernel);
    clReleaseProgram(program);
    clReleaseCommandQueue(Command_queue);
    clReleaseMemObject(bufImg);
    clReleaseMemObject(bufRst);
    clReleaseContext(context);

    free(image);
    free(histogram_results);
    free(platforms);
    free(devices);

    inFile.close();
    outFile.close();
    return 0;
}
