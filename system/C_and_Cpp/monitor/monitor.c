#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <time.h>
#include <sys/types.h>
#include <sys/stat.h>
//option.
#include <ncurses.h>

#define DISK_FILE "/proc/diskstats"
#define MEM_FILE "/proc/meminfo"
#define CPU_FILE "/proc/stat"
#define SECTOR_SIZE "/sys/block/sda/queue/hw_sector_size"
#define ACTIVE_MONITOR true
#define UPDATE_SPEED 1

/* define Color */
#define GRE "\x1B[32m"
#define RED "\x1B[31m"
#define WHT "\x1B[37m"
#define RESET "\033[0m"

/* define structure */

struct cpu_stat
{
    unsigned long user;
    unsigned long nice;
    unsigned long sys;
    unsigned long idle;
    unsigned long iowait;
    unsigned long irq;
    unsigned long softirq;
};

struct mem_stat
{
    unsigned long total;
    unsigned long available;
    unsigned long free, buffers, cached;
};

struct disk_stat
{
    int major, minor;
    char dev_name[64];
    unsigned long read_completed;
    unsigned long read_merged;
    unsigned long read_sector;
    unsigned long read_spend_time;
    unsigned long write_completed;
    unsigned long write_merged;
    unsigned long write_sector;
    unsigned long write_speed_time;
    unsigned long io_in_progress;
    unsigned long time_spenton_io;
    unsigned long time_weighted;
};

struct cpu_stat get_cpu_stat()
{
    FILE *fp;
    char cpu[4];
    struct cpu_stat c_stat;

    fp = fopen(CPU_FILE, "r");
    fscanf(fp, "%4s %ld %ld %ld %ld %ld %ld %ld", cpu, &c_stat.user, &c_stat.nice, &c_stat.sys,
            &c_stat.idle, &c_stat.iowait, &c_stat.irq, &c_stat.softirq);
    fclose(fp);

    return c_stat;
}

struct mem_stat get_mem_stat()
{
    FILE *fp;
    struct mem_stat m_stat;

    fp = fopen(MEM_FILE, "r");
    fscanf(fp, "MemTotal:%ld kB\n", &m_stat.total);
    fscanf(fp, "MemFree:%ld kB\n", &m_stat.free);
    fscanf(fp, "MemAvailable:%ld kB\n", &m_stat.available);
    fscanf(fp, "Buffers:%ld kB\n", &m_stat.buffers);
    fscanf(fp, "Cached:%ld kB\n", &m_stat.cached);
    fclose(fp);
    return m_stat;
}

struct disk_stat get_disk_stat()
{
    FILE *fp;
    struct disk_stat d_stat;

    fp = fopen(DISK_FILE, "r");

    char line[128];
    while(fgets(line, sizeof(line), fp) != NULL)
    {
        sscanf(line, "%i %i %s %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu",
                &d_stat.major, &d_stat.minor, d_stat.dev_name,
                &d_stat.read_completed, &d_stat.read_merged, &d_stat.read_sector, &d_stat.read_spend_time,
                &d_stat.write_completed, &d_stat.write_merged, &d_stat.write_sector, &d_stat.write_speed_time,
                &d_stat.io_in_progress, &d_stat.time_spenton_io, &d_stat.time_weighted);
        if(!strcmp(d_stat.dev_name, "sda")) break;
    }
    fclose(fp);
    return d_stat;
}

void print_cpu_usage(struct cpu_stat *cpu_pre, struct cpu_stat *cpu_curr)
{
    float total;
    float idle;

    total = (cpu_curr->user + cpu_curr->nice + cpu_curr->sys + cpu_curr->idle + cpu_curr->iowait + cpu_curr->irq + cpu_curr->softirq) -
            (cpu_pre->user + cpu_pre->nice + cpu_pre->sys + cpu_pre->idle + cpu_pre->iowait + cpu_pre->irq + cpu_pre->softirq);

    idle = cpu_curr->idle - cpu_pre->idle;
    printf("[*] CPU usage is " GRE "%.6f" RESET " || " , (total - idle) / total * 100);
    return;
}

void print_mem_usage(struct mem_stat *mem_pre, struct mem_stat *mem_curr)
{
    int change = (mem_pre->available - mem_curr->available);
    double percentage = ( (double)(mem_curr->total - mem_curr->available) / (double)mem_curr->total ) * 100;
    double used_mem = (mem_curr->total - mem_curr->available);

    printf("Memory Usage: %.3lf MB / %.3lf MB(%.3f%%)", used_mem * 0.001, mem_curr->total * 0.001, percentage) ;
    if(change > 0) printf(GRE "(%.2lf MB)", change * 0.001); // green means memory was reclaimed (freed).
    if(change == 0) printf(WHT "(--)");
    if(change < 0) printf(RED "(%.2lf MB)", change * 0.001); // red means memory was consumed (used).
    printf(RESET " || " RESET);
    return;
}

int get_sector_size()
{
    unsigned long sector_size;
    FILE *sfp;
    sfp = fopen(SECTOR_SIZE, "r");
    fscanf(sfp, "%ld", &sector_size);
    fclose(sfp);
    return sector_size;
}

void print_disk_usage(struct disk_stat *disk_pre, struct disk_stat *disk_curr)
{
    int sector_size = get_sector_size();
    unsigned long write_diff = disk_curr->write_sector - disk_pre->write_sector;
    unsigned long read_diff = disk_curr->read_sector - disk_pre->read_sector;

    double write_speed = (double)((write_diff * sector_size) / 1024);
    double read_speed = (double)((read_diff * sector_size) / 1024);

    //printf("Disk Write Speed: %.3lf MB/s | Disk Read Speed: %.3lf MB/s", write_speed, read_speed);
    printf("  Disk Write Speed: %.3lf KB/s  |  ", write_speed);
    printf("Disk Read Speed: %.3lf KB/s", read_speed);

    return;
}

int main(void)
{
    printf("Monitor Program (Just For Linux): \n");
    printf("\n");

    // define the structure we will use.
    struct mem_stat mem_curr = get_mem_stat();
    struct mem_stat mem_pre;

    struct cpu_stat cpu_curr = get_cpu_stat();;
    struct cpu_stat cpu_pre;

    struct disk_stat disk_curr = get_disk_stat();;
    struct disk_stat disk_pre;

    /* Get usage and calculate change since last update. */
    if(ACTIVE_MONITOR == true)
    {
        while(1)
        {
            /* CPU Part */
            cpu_pre = cpu_curr;
            cpu_curr = get_cpu_stat();

            /* Memory Part */
            mem_pre = mem_curr;
            mem_curr = get_mem_stat();

            /* Disk Part */
            disk_pre = disk_curr;
            disk_curr = get_disk_stat();

            /* print functions */
            printf("\r");
            print_cpu_usage(&cpu_pre, &cpu_curr);
            print_mem_usage(&mem_pre, &mem_curr);
            print_disk_usage(&disk_pre, &disk_curr);
            printf("        " RESET);
            fflush(stdout);

            sleep(UPDATE_SPEED) ;
        }
    }

    return 0;
}
