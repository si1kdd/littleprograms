package main

import (
        "fmt"
        "time"
        "log"
)

var finish = make(chan int)
var res = make([]int, 0, 100)

func listen_channel(queue chan int, flag bool) {
        for output := range queue {
                time.Sleep(1000)
                res = append(res, output)
                fmt.Println(flag)
        }
        fmt.Println("[+] Finish !")
}

func main() {

}
